/**
 * 
 */
package org.com1028.coursework.tests;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.Test;

import com.com1028.assignment.Main;

/**
 * @author sebas
 *
 */
public class Main_Test {

	/**
	 * Test method for {@link com.com1028.assignment.Main#Query(int)}.
	 */
	@Test
	public void testQuery() {
		
		Main.Query(1);
		if (Main.carsList.isEmpty()) {
			fail("Query 1 failed");
		}
		Main.Query(2);
		if(Main.srepList.isEmpty()) {
		fail("First part of Query 2 failed");
		}else if ((!Main.srepList.isEmpty()) && (Main.custList.isEmpty() || Main.custnorepList.isEmpty())) {
			fail("Second part of Query 2 failed");
		}
		Main.Query(3);
		if (Main.productList.isEmpty()) {
			fail("First Part of Query 3 failed");
		}
		if(Main.ordersList.isEmpty()) {
			fail("Second Part of Query 3 failed");
		}
	}

	/**
	 * Test method for {@link com.com1028.assignment.Main#plListtoString(java.util.List)}.
	 */

	/**
	 * Test method for {@link com.com1028.assignment.Main#profitReport()}.
	 */
	@Test
	public void testProfitReport() {
		Main.Query(3);
		Main.profitReport();
		for (double markup: Main.markups ) {
			if (markup<2.0) {
				fail("Markup less than 2");
			}
		}
		
	}

	/**
	 * Test method for {@link com.com1028.assignment.Main#main(java.lang.String[])}.
	 */
	@Test
	public void testMainnull() throws IllegalArgumentException {
		String input = "";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);
		//I do not know how to test it with simulated user inputs
	}

}
