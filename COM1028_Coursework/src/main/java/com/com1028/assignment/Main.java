/**
 * 
 */
package com.com1028.assignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * @author sc01879
 *
 */
public class Main {
	// made public to access it when testing
	public static List<Productlines> carsList = new ArrayList<Productlines>();
	public static List<Employees> srepList = new ArrayList<Employees>();
	public static List<Customers> custList = new ArrayList<Customers>();
	public static List<Customers> custnorepList = new ArrayList<Customers>();
	public static List<Products> productList = new ArrayList<Products>();
	public static List<OrderDetails> ordersList = new ArrayList<OrderDetails>();
	public static List<Double> markups = new ArrayList<Double>(); // used for testing profitreport

	public static void Query(int i) { //query to get values from the database
		try {

			// Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/classicmodels?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root", "Avancaemundo!1"); 
			Statement stmt = conn.createStatement();
			//divide it into 3 parts for each requirement
			if (i == 1) {  //First requirement query
				String sql = "SELECT * FROM ProductLines"; //Select all from ProductLines
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					String pl = rs.getString("productLine");
					String td = rs.getString("textDescription");
					String hd = rs.getString("htmlDescription");
					Blob img = rs.getBlob("image");
					Productlines a = new Productlines(pl, td, hd, img);
					if (a.is_car()) { //check if its a car and adds it to the car list
						carsList.add(a);
					}
				}
			} else if (i == 2) {//Second requirement query
				String sql = "SELECT employeeNumber, lastName, firstName, email, jobTitle FROM Employees"; //select just what is needed and what seemed important
				ResultSet rs = stmt.executeQuery(sql);
				while (rs.next()) {
					int eNumber = rs.getInt("employeeNumber");
					String lName = rs.getString("lastName");
					String fName = rs.getString("firstName");
					String email = rs.getString("email");
					String jTitle = rs.getString("jobTitle");
					Employees a = new Employees(eNumber, lName, fName, email, jTitle);
					if (jTitle.equals("Sales Rep")) { //removes any unneeded rows by only adding sales representatives to the list
						srepList.add(a);

					}
				} //second query to get a different table
				String sql2 = "SELECT customerNumber,customerName,salesRepEmployeeNumber FROM Customers";
				ResultSet rs2 = stmt.executeQuery(sql2);
				while (rs2.next()) {
					int cNumber = rs2.getInt("customerNumber");
					String cName = rs2.getString("customerName");
					int srepNumber = rs2.getInt("salesRepEmployeeNumber");
					Customers d = new Customers(cNumber, cName, srepNumber);
					if (srepNumber == 0) { //some customers do not have representatives so i divided them into 2 lists
						custnorepList.add(d);
					} else {
						custList.add(d);
					}
				}

			} else if (i == 3) {//Third requirement query
				String sql = "SELECT productCode, productName, buyPrice, productLine FROM Products"; //select just what is needed and what seemed important
				ResultSet rs = stmt.executeQuery(sql);
				while (rs.next()) {
					String pCode = rs.getString("productCode");
					String pName = rs.getString("productName");
					double buyPrice = rs.getDouble("buyPrice");
					String pLine = rs.getString("productLine");
					Products a = new Products(pCode, pName, buyPrice, pLine);
					productList.add(a);

				}
				String sql2 = "SELECT orderNumber, productCode, priceEach FROM OrderDetails";
				ResultSet rs2 = stmt.executeQuery(sql2);
				while (rs2.next()) {
					int oNumber = rs2.getInt("orderNumber");
					String pCode = rs2.getString("productCode");
					double priceEach = rs2.getDouble("priceEach");
					OrderDetails a = new OrderDetails(oNumber, pCode, priceEach);
					ordersList.add(a);
				}
			}//close connection and statement 
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void plListtoString(List<Productlines> list) { //Just prints any object's toString that is on the list
		int i = 0;
		while (i < list.size()) {
			Productlines a = list.get(i);
			System.out.println(a.toString());
			i++;
		}
	}

	public static void accountRepReport() {//Outputs all the sales representatives and their customers
		int i = 0;
		while (i < srepList.size()) {
			Employees a = srepList.get(i);
			System.out.println();
			System.out.println(a.toString() + " represents:");
			int j = 0;
			while (j < custList.size()) {
				Customers b = custList.get(j);
				if (b.getSalesRepEmployeeNumber() == a.getEmployeeNumber()) {
					System.out.println(b.toString());
				}
				j++;
			}
			i++;
		}

		System.out.println("These customers have no sales representative: "); //Outputs all the customers with no representatives
		i = 0;
		while (i < custnorepList.size()) {
			Customers c = custnorepList.get(i);
			System.out.println(c.toString());
			i++;
		}
	}

	public static void profitReport() { //Outputs all orders that have 100% markup
		int i = 0;

		while (i < productList.size()) {
			List<String> sList = new ArrayList<String>();
			Products a = productList.get(i);

			int j = 0;
			while (j < ordersList.size()) {
				OrderDetails b = ordersList.get(j);
				if (b.getProductCode().equals(a.getProductCode())) {
					double markup = (b.getPriceEach() / a.getBuyPrice());//check if the sell price/ buy price equal at least 2  
					if (markup >= 2.0) {
						markups.add(markup);
						sList.add(b.toString());
					}
				}
				j++;
			}
			if (!sList.isEmpty()) {
				System.out.println();
				System.out.println(a.toString() + " and sold with atleast 100% markup in these orders:  ");
				int f = 0;
				while (f < sList.size()) {
					System.out.println(sList.get(f).toString());
					f++;
				}
			}
			i++;
		}
	}

	public static void main(String[] args) throws IllegalArgumentException {
		final String Regex = "[1-3]{1}";//prevents illegal arguments from being input
		Scanner a = new Scanner(System.in); // Create a Scanner object
		System.out.println("Which requirement do you want? (1,2,3)");
		String b = a.nextLine();
		boolean check = Pattern.matches(Regex, b);
		if (check == false) {
			throw new IllegalArgumentException("ERROR! INVALID INPUT! PLEASE INPUT A NUMBER FROM 1 TO 3!");
		}
		int i = Integer.parseInt(b); // Read user input
		if (i == 1) { //directs to each requirement's code
			Query(i);
			plListtoString(carsList);
		} else if (i == 2) {
			Query(i);
			accountRepReport();
		} else if (i == 3) {
			Query(i);
			profitReport();
		} else {
			System.out.println("ERROR! OPTION NOT AVAILABLE!");
		}

	}

}
