/**
 * 
 */
package com.com1028.assignment;

/**
 * @author sc01879
 *
 */
public class Employees {
	private int employeeNumber = 0;
	private String lastName = null;
	private String firstName = null;
	private String email = null;
	private String jobTitle =null;
	public Employees(int employeeNumber,String lastName,String firstName,String email,String jobTitle) {
		super();
		this.employeeNumber= employeeNumber;
		this.lastName = lastName;
		this.firstName= firstName;
		this.email = email;
		this.jobTitle= jobTitle;
	}
	@Override
	public String toString() {
		return "Employee (" + employeeNumber + ") " + firstName + " " + lastName
				+ " (" + email + ")";
	}
	public int getEmployeeNumber() {
		return employeeNumber;
	}
	public String getLastName() {
		return lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getEmail() {
		return email;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	
}
