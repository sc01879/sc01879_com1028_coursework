/**
 * 
 */
package com.com1028.assignment;

/**
 * @author sc01879
 *
 */
public class Customers {
	private int customerNumber=0;
	private String customerName = null;
	private int salesRepEmployeeNumber = 0;
	public Customers(int customerNumber,String customerName,int salesRepEmployeeNumber) {
		super();
		this.customerNumber = customerNumber;
		this.customerName = customerName;
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}
	public int getCustomerNumber() {
		return customerNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public int getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}
	@Override
	public String toString() {
		return  (customerName + " | customer number = " + customerNumber);
	}
	
}
