/**
 * 
 */
package com.com1028.assignment;

/**
 * @author sc01879
 *
 */
public class OrderDetails {
private int orderNumber = 0;
private String productCode=null;
private double priceEach = 0.0;
public OrderDetails(int orderNumber, String productCode, double priceEach) {
	super();
	this.orderNumber = orderNumber;
	this.productCode = productCode;
	this.priceEach = priceEach;
	
}
public int getOrderNumber() {
	return orderNumber;
}
public String getProductCode() {
	return productCode;
}
public double getPriceEach() {
	return priceEach;
}
@Override
public String toString() {
	return "Order Number: " + orderNumber + ", sold at " + priceEach;
}

}
