/**
 * 
 */
package com.com1028.assignment;


import java.sql.Blob;

/**
 * @author sebas
 *
 */
public class Productlines {
	private String productLine = null;
	private String textDescription = null;
	private String htmlDescription = null;
	private Blob image = null;
	public Productlines(String productLine, String textDescription,String htmlDescription, Blob image){
		super();
		this.productLine = productLine;
		this.textDescription = textDescription;
		this.htmlDescription= htmlDescription;
		this.image = image;
}
	public boolean is_car() { //checks if the productLine string contains the word Car
		if (this.productLine.contains("Car")) {
			return true;
		}else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "[Name=" + productLine + " || textDescription=" + textDescription
				+ " || htmlDescription=" + htmlDescription + " || image=" + image + "]";
	}

}
