/**
 * 
 */
package com.com1028.assignment;

/**
 * @author sc01879
 *
 */
public class Products {
	private String productCode = null;
	private String productName = null;
	private double buyPrice = 0;
	private String productLine =null;
	public Products(String productCode,String productName,double buyPrice,String productLine) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.buyPrice = buyPrice;
		this.productLine = productLine;
		
	}
	public String getProductCode() {
		return productCode;
	}
	public double getBuyPrice() {
		return buyPrice;
	}
	@Override
	public String toString() {
		return "The product: " + productName + " (" + productCode + ") from productLine: " + productLine
				+ ", was bought by :" + buyPrice;
	}
}
